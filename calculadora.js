/* 
    * * * * * * * * * * * * * * * *
    *  1. C A L C U L A D O R A   *
    * * * * * * * * * * * * * * * *
    
    Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones. 

        - El programa debe recibir dos números (n1, n2).

        - Debe existir una variable que permita seleccionar de alguna forma el tipo de operación (suma, resta, multiplicación 
          o división).

        - Opcional: agrega una operación que permita elevar n1 a la potencia n2.

*/
let n1 = 3
let n2 = 6
let operacion = ('n1 elevado a n2')   //elejir entre: (('+'),('-'),('*'),('/'),('n1 elevado a n2')) 

function calculadora(operacion, n1, n2) {
    if (operacion === 'n1 elevado a n2') {
       return Math.pow(n1, n2);
    }
    if (operacion === '+') {
        return n1 + n2;
    }
    if (operacion === '-') {
        return n1 - n2;
    }
    if (operacion === '*') {
        return n1 * n2;
    }
    if (operacion === '/' && n2 == 0) {
        console.log('¡¡¡ ERROR !!! No se puede dividir entre cero');
    } else {
        return n1 / n2;
    }




}
console.log('El resultado es: ' + calculadora(operacion, n1, n2));


