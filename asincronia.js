/*
    * * * * * * * * * * * * * * * * * * * * * * *
    *  3. ASINCRONIA                            *
    * * * * * * * * * * * * * * * * * * * * * * *

    En este ejercicio se comprobará la competencia de los alumnos en el concepto de asincronía 
    Se proporcionan 3 archivos  csv separados por comas y se deberán bajar asíncronamente (promises) 
    
    A la salida se juntarán los registros de los 3 archivos en un array que será el parámetro de entrada 
    de la funcion findIPbyName(array, name ,surname) que buscará una entrada en el array y devolverá la IP correspondiente

    Una vez hallada la IP ha de mostrarse por pantalla

    para llamar a la función utilizad el nombre Cari Wederell
   
*/
const axios = require('axios');


function findIPbyName(array, name ,surname)

for (let line of array) {
  const fieldsOfLine = line.split(';');
  if (fieldsOfLine.length === 1) {
    continue;
  }
  if ((fieldsOfLine[IP_POSITION].toUpperCase().indexOf(IP.toUpperCase()) !== -1) ||
  (fieldsOfLine[IP_POSITION].length !== 0 && IP.toUpperCase().indexOf(fieldsOfLine[IP_POSITION].toUpperCase()) !== -1)) {
  counter++;
}


getIPData = async (IP) => {
MOCK_DATA1 = await axios.get(/home/hab15/Desktop/examen/files/MOCK_DATA1.csv);
const IP_MOCK_DATA1 = findIPbyName(MOCK_DATA1 .data.split('\n'), IP);

MOCK_DATA2 = await axios.get(/home/hab15/Desktop/examen/files/MOCK_DATA2.csv);
const IP_MOCK_DATA2 = findIPbyName(MOCK_DATA2 .data.split('\n'), IP);

MOCK_DATA3 = await axios.get(/home/hab15/Desktop/examen/files/MOCK_DATA3.csv);
const IP_MOCK_DATA3 = findIPbyName(MOCK_DATA3 .data.split('\n'), IP);

return [IP_MOCK_DATA1 , IP_MOCK_DATA2 , IP_MOCK_DATA2 ]
}


getIPData ('Cari' , 'Wederell')
.then((IP) => {
  console.log('El IP es: ', IP);
})


}



